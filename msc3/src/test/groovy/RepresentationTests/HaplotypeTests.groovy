/*
 Copyright (C) 2015  Matthias Samwald

 This software collection (genomic-cds) is available under a dual licensing
 whereby use of the software in projects that are licensed so as to be compatible
 with AGPL Version 3 may use the software under the terms of that license.
 See LICENSE.md or contact matthias.samwald@meduniwien.ac.at for further details
 on the dual licensing.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package RepresentationTests

import org.safetycode.msc3.representation.Haplotype
import org.safetycode.msc3.representation.MSCManager
import spock.lang.Specification

import java.lang.reflect.Field
/**
 * Created by Sebastian on 06.11.2015.
 */
class HaplotypeTests extends Specification{

    MSCManager manager = MSCManager.getInstance()

    def "there should be 689 haplotypes"() {
        expect:
        manager.getHaplotypes().size() == 689
    }

    void hasNoNullFields (Haplotype haplotype) {
        for(Field field : haplotype.getClass().getDeclaredFields()) {
            field.setAccessible(true) // necessary to access private fields
            assert field.get(haplotype) != null,
                    String.format("%s %s should not be null", haplotype.getName(), field.getName())
        }
    }

    def "no haplotype should have a null field"() {
        expect:
        for (Haplotype haplotype : manager.getHaplotypes()) {
            hasNoNullFields(haplotype)
        }
    }
}
