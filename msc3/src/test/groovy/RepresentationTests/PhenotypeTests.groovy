/*
 Copyright (C) 2015  Matthias Samwald

 This software collection (genomic-cds) is available under a dual licensing
 whereby use of the software in projects that are licensed so as to be compatible
 with AGPL Version 3 may use the software under the terms of that license.
 See LICENSE.md or contact matthias.samwald@meduniwien.ac.at for further details
 on the dual licensing.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package RepresentationTests

import org.safetycode.msc3.representation.MSCManager
import org.safetycode.msc3.representation.Phenotype
import spock.lang.Specification

import java.lang.reflect.Field

/**
 * Created by Sebastian on 06.11.2015.
 */
class PhenotypeTests extends Specification {

    MSCManager manager = MSCManager.getInstance()

    def "there should be 75 phenotypes"() {
        expect:
        manager.getPhenotypes().size() == 75
    }

    void hasNoNullFields (Phenotype phenotype) {
        for(Field field : phenotype.getClass().getDeclaredFields()) {
            field.setAccessible(true) // necessary to access private fields
            assert field.get(phenotype) != null,
                    String.format("%s %s should not be null", phenotype.getName(), field.getName())
        }
    }

    def "no phenotype should have a null field"() {
        expect:
        for (Phenotype phenotype : manager.getPhenotypes()) {
            hasNoNullFields(phenotype)
        }
    }

}
