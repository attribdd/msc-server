/*
 Copyright (C) 2015  Matthias Samwald

 This software collection (genomic-cds) is available under a dual licensing
 whereby use of the software in projects that are licensed so as to be compatible
 with AGPL Version 3 may use the software under the terms of that license.
 See LICENSE.md or contact matthias.samwald@meduniwien.ac.at for further details
 on the dual licensing.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.safetycode.msc3.populate;

import org.safetycode.msc3.representation.Gene;
import org.safetycode.msc3.representation.Haplotype;
import org.safetycode.msc3.util.Common;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;

/**
 * Created by Sebastian on 17.06.2015.
 */
public class HaplotypePopulator extends Populator{
    private Properties properties;
    private HashMap<String, Haplotype.HaplotypeBuilder> builders = new HashMap<String, Haplotype.HaplotypeBuilder>();

    public HaplotypePopulator(String path) throws IOException {
        this.properties = readProperties(path);
    }

    @Override
    public void populate() {
        prepareHaplotypes();
        createHaplotypes();
    }

    private void prepareHaplotypes() {
        Enumeration e = this.properties.propertyNames();
        while (e.hasMoreElements()) {
            String key = (String) e.nextElement();
            String value = this.properties.getProperty(key);
            prepareHaplotype(key, value);
        }
    }

    private void prepareHaplotype(String key, String value) {
        String[] tokens = key.split("\\.");
        String gene = tokens[0];
        String haplotype = tokens[1];
        String property = tokens[2];

        Haplotype.HaplotypeBuilder builder = getBuilder(gene, haplotype);
        addProperty(builder, property, value);
    }

    private void addProperty(Haplotype.HaplotypeBuilder builder, String property, String value) {
        if (property.equals("name")) {
            builder.setName(value);
        }
        else if (property.equals("urlFragment")) {
            builder.setUrlFragment(value);
        }
        else {
            System.err.println("Found wrong property: " + property);
        }
    }

    private Haplotype.HaplotypeBuilder getBuilder(String gene, String haplotype) {
        String key = Common.toMarkerId(gene, haplotype);
        if (this.builders.containsKey(key)) {
            return this.builders.get(key);
        } else {
            Haplotype.HaplotypeBuilder builder = new Haplotype.HaplotypeBuilder()
                    .setId(key)
                    .setGene(Gene.getBySymbol(gene));
            this.builders.put(key, builder);
            return builder;
        }
    }

    private void createHaplotypes() {
        for (Haplotype.HaplotypeBuilder builder : this.builders.values()) {
            builder.build();
        }
    }
}
