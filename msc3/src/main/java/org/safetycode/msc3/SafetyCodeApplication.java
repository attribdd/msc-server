/*
 Copyright (C) 2015  Matthias Samwald

 This software collection (genomic-cds) is available under a dual licensing
 whereby use of the software in projects that are licensed so as to be compatible
 with AGPL Version 3 may use the software under the terms of that license.
 See LICENSE.md or contact matthias.samwald@meduniwien.ac.at for further details
 on the dual licensing.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.safetycode.msc3;

import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.resource.Directory;
import org.restlet.routing.Router;
import org.safetycode.msc3.representation.MSCManager;
import org.safetycode.msc3.resources.GenerateServerResource;
import org.safetycode.msc3.resources.PocketCardServerResource;
import org.safetycode.msc3.resources.ProfileServerResource;
import org.safetycode.msc3.resources.RootServerResource;

/**
 * Created by Sebastian on 06.05.2015.
 */
public class SafetyCodeApplication extends Application {
    MSCManager manager;

    public SafetyCodeApplication() {
        this.manager = MSCManager.getInstance();
    }

    /**
     * Creates a root Restlet that will receive all incoming calls.
     */
    @Override
    public synchronized Restlet createInboundRoot() {
        Router router = new Router(getContext());
        routeDirectories(router);
        routeResources(router);
        return router;
    }

    private void routeDirectories(Router router) {
        router.attach("/_assets/", new Directory(getContext(), "war:///_assets/"));
        router.attach("/api-docs/", new Directory(getContext(), "war:///api-docs/"));
        router.attach("/docs/", new Directory(getContext(), "war:///docs/"));
        router.attach("/images/", new Directory(getContext(), "war:///images/"));
        router.attach("/css/", new Directory(getContext(), "war:///css/"));
        router.attach("/js/", new Directory(getContext(), "war:///js/"));
    }

    private void routeResources(Router router) {
        router.attach("/", RootServerResource.class);
        router.attach("/{assayId}/{profileId}", ProfileServerResource.class);
        router.attach("/cards/{assayId}/{profileId}", PocketCardServerResource.class);
        router.attach("/generate", GenerateServerResource.class);
    }

    /*
    public static void main(String... args) throws Exception {

        SafetyCodeApplication app = new SafetyCodeApplication();
        System.out.println("Starting server at http://localhost:8080...");
        Component mscServer = new Component();
        mscServer.getServers().add(Protocol.HTTP, 8080);
        mscServer.getDefaultHost().attach(app);
        mscServer.start();

        /*
        MSCManager manager = MSCManager.getInstance();
        System.out.println("We can get all guidelines in the system...");
        List<Guideline> guidelines = manager.getGuidelines();
        System.out.println(guidelines.size() + " guidelines: " + guidelines);

        System.out.println("We can get all genes in the system...");
        List<Gene> genes = manager.getGenes();
        Gene gene = genes.get(22);
        System.out.println(genes.size() + " genes: " + genes);

        System.out.println("And their respective haplotypes...");
        List<Haplotype> haplotypes = gene.getHaplotypes();
        Haplotype haplotype = haplotypes.get(0);
        System.out.println(gene + ": " + haplotypes);

        System.out.println("... and phenotypes...");
        List<Phenotype> phenotypes = gene.getPhenotypes();
        Phenotype phenotype = phenotypes.get(0);
        System.out.println(gene + ": " + phenotypes);

        System.out.println("The markers for a profile are haplotypes and phenotypes...");
        List<String> markers = Profile.availableMarkers;
        System.out.println(markers.size() + " markers: " + markers);

        System.out.println("And we can get a map of all markers to default values...");
        Map<String, String> emptyMarkers = Profile.emptyMarkers;
        System.out.println(emptyMarkers);

        System.out.println("A map from urlFragments to phenotypes for a single gene...");
        System.out.println(gene.getPhenotypesByUrlFragment());

        System.out.println("Creating a profile...");
        // CPIC CYP2C19 ultrarapid metabolizer, *17/*17 & CPIC DPYD deficiency, *2A/*13
        Profile profile = new Profile("710T0TC5010E");
    }
    */
}
