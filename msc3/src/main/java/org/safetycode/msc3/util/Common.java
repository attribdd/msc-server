/*
 Copyright (C) 2015  Matthias Samwald

 This software collection (genomic-cds) is available under a dual licensing
 whereby use of the software in projects that are licensed so as to be compatible
 with AGPL Version 3 may use the software under the terms of that license.
 See LICENSE.md or contact matthias.samwald@meduniwien.ac.at for further details
 on the dual licensing.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.safetycode.msc3.util;

import com.itextpdf.text.BaseColor;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Sebastian on 13.05.2015.
 */
public class Common {

    public static final String URL_PREFIX = "http://safety-code.org/msc3/";

    /**
     *  The current version of the safetycode system.
     */
    public static final String VERSION = "v0.3";

    /**
     * Digits used to represent a base 64 number for a safetycode number.
     */
    public static final String BASE_DIGITS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_";

    /**
     * A JavaScript engine, mainly used to evaluate Guideline criteria.
     */
    public static ScriptEngine JS_ENGINE = new ScriptEngineManager().getEngineByName("js");

    /**
     * The width of the QR code in pixels (including the white area around it, but not the frame).
     */
    public final static int QR_WIDTH = 174;

    /**
     * The height of the QR code in pixels (including the white area around it, but not the frame).
     */
    public final static int QR_HEIGHT = 177;

    /**
     * The String representing a important modification.
     */
    public static String IMPORTANT_MODIFICATION = "Important modification";

    /**
     * The width of the table on the back side of the pocket card.
     */
    public static int CARD_COLUMN_WIDTH = 230;

    /**
     * The relative width of the columns of the table on the back side of the pocket card.
     */
    public static float[] CARD_COLUMN_WIDTHS = {0.3f, 0.7f};

    /**
     * How much additional space should be between newlines in the lab information on the pocket card?
     */
    public static final int LAB_NEWLINE_SPACING = 1;

    /**
     * The color of the borders of the table on the back of the pocket card.
     */
    public static final BaseColor POCKET_CARD_TABLE_BORDER_COLOR = new BaseColor(76, 139, 179);

    /**
     * The maximum height of the table on the back of the pocket card.
     */
    public static final float POCKET_CARD_MAX_TABLE_HEIGHT = 119f;

    // all paths are relative to WebContent/WEB-INF/classes/
    // PROPERTY FILE PATHS
    public static String GENES_PROPERTIES = "/config/genes.properties";
    public static String HAPLOTYPES_PROPERTIES = "/config/haplotypes.properties";
    public static String PHENOTYPES_PROPERTIES = "/config/phenotypes.properties";
    public static String GUIDELINES_PROPERTIES = "/config/guidelines.properties";
    public static String DRUGS_PROPERTIES = "/config/drugs.properties";

    // TEMPLATE PATHS
    public static final String PROFILE_TEMPLATE = "profile_template.html";
    public static final String GENERATE_TEMPLATE = "generate_template.html";
    public static final String POCKET_CARD_TEMPLATE = "pocket_card_template.pdf";

    // OTHER PATHS
    /**
     * The name of the image that is used to frame the QR code.
     */
    public final static String QR_FRAME = "../../images/safety-code-frame-2014.png";

    // METHODS
    /**
     * Takes a list and returns the String representation without surrounding brackets.
     */
    public static String prettyListString(List list) {
        ArrayList<String> stringList = new ArrayList<String>();
        for(Object o : list) {
            stringList.add(o.toString());
        }
        Collections.sort(stringList);
        String text = stringList.toString();
        return text.substring(1, text.length()-1);
    }

    public static URL getResource(String path) {
        return new Common().getClass().getClassLoader().getResource(path);
    }

    public static String toMarkerId(String gene, String suffix) {
        return gene + "_" + suffix;
    }

    public static String prefixWithBaseUrl(String str) {
        return URL_PREFIX + str;
    }
}
