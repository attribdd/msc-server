/*
 Copyright (C) 2015  Matthias Samwald

 This software collection (genomic-cds) is available under a dual licensing
 whereby use of the software in projects that are licensed so as to be compatible
 with AGPL Version 3 may use the software under the terms of that license.
 See LICENSE.md or contact matthias.samwald@meduniwien.ac.at for further details
 on the dual licensing.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.safetycode.msc3.resources;

import com.google.zxing.WriterException;
import org.apache.commons.io.IOUtils;
import org.restlet.data.MediaType;
import org.restlet.representation.ByteArrayRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;
import org.safetycode.msc3.exceptions.GeneticUrlValidationException;
import org.safetycode.msc3.representation.Drug;
import org.safetycode.msc3.representation.Gene;
import org.safetycode.msc3.representation.Guideline;
import org.safetycode.msc3.representation.Profile;
import org.safetycode.msc3.util.Common;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.List;

/**
 * Created by Sebastian on 03.08.2015.
 */
public class ProfileServerResource extends ServerResource {
    private String assayId;
    private String profileId;
    private Profile profile;

    @Override
    protected void doInit() throws ResourceException {
        this.assayId = getAttribute("assayId");
        this.profileId = getAttribute("profileId");
        this.profile = new Profile(this.assayId, this.profileId);
    }
/*
    protected Profile tryCreateProfile() {
        Profile profile = new Profile(this.assayId, this.profileId);
        return profile;
    }

    protected static Profile tryCreateProfile(String assayId, String profileId) {
        Profile profile = new Profile(assayId, profileId);
        return profile;
    } */

    @Get("html")
    public String getHtml() throws IOException, GeneticUrlValidationException {
        return getHtmlTemplate().replace("${recommendations}", getRecommendationsAsHtml())
                                .replace("${phenotypes_and_haplotypes}", getGeneticDataAsHtml());
    }

    /**
     * Returns the barcode image for the profile, including a frame around it.
     * @return The barcode image
     */
    @Get("png")
    public ByteArrayRepresentation getPng() throws IOException, WriterException {
        ByteArrayOutputStream baos = addCodeGraphics(new ByteArrayOutputStream(), profile.getCodeImage(), profile.getFrameImage());
        return new ByteArrayRepresentation(baos.toByteArray(), MediaType.IMAGE_PNG);
    }

    /**
     * Adds the barcode graphics (including the frame around it) to the given ByteArrayOutputStream and returns it.
     * @param baos The target ByteArrayOutputStream
     * @param codeImage The image of the QR code
     * @param frameImage The image of the frame around the QR code
     * @return
     */
    private ByteArrayOutputStream addCodeGraphics(ByteArrayOutputStream baos,
                                                  BufferedImage codeImage,
                                                  BufferedImage frameImage) throws IOException {
        BufferedImage finalImage = new BufferedImage(frameImage.getWidth(), frameImage.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics g = finalImage.getGraphics();
        g.drawImage(frameImage, 0, 0, null); // draw the frame and logo of the Medicine Safety Code
        g.drawImage(codeImage, 12, 30, null); // position barcode inside frame
        ImageIO.write(finalImage, "PNG", baos);

        return baos;
    }

    /**
     * Returns the HTML containing the guideline recommendations for this profile.
     * @return the HTML for the guideline recommendations
     */
    private String getRecommendationsAsHtml() throws IOException {
        StringBuilder result = new StringBuilder();
        prepareRecommendations(result);
        addDrugEntries(result, this.profile.getImportantGuidelinesByDrug().entrySet());
        finishRecommendations(result);
        return result.toString();
    }

    private void addDrugEntries(StringBuilder result, Set<Map.Entry<String, List<Guideline>>> entrySet) {
        if (entrySet.size() == 0) {
            result.append("<br /> No critical guidelines apply to this genetic profile.<br /><br />");
        }
        else {
            for (Map.Entry<String, List<Guideline>> entry : sortedCopy(entrySet)) {
                addDrugEntry(result, entry);
            }
        }
    }

    /**
     * Takes a set and returns a sorted list of the same entries.
     * @param set
     */
    private List<Map.Entry<String, List<Guideline>>> sortedCopy(Set<Map.Entry<String, List<Guideline>>> set) {
        List copy = new ArrayList(set);
        Collections.sort(copy, new Comparator<Map.Entry<String, List<Guideline>>>() {
            @Override
            public int compare(Map.Entry<String, List<Guideline>> o1, Map.Entry<String, List<Guideline>> o2) {
                return o1.getKey().compareTo(o2.getKey());
            }
        });
        return copy;
    }

    private void prepareRecommendations(StringBuilder result) {
        result.append("<div data-role='collapsible-set'>");
        result.append("<ul data-role='listview' data-inset='true' data-filter='true' data-filter-placeholder='Filter substance list...'>");
        result.append("<li data-role='list-divider'>Critical guidelines</li>");
    }

    private void finishRecommendations(StringBuilder result) {
        result.append("</ul></div>");
    }

    private void addDrugEntry(StringBuilder result, Map.Entry<String, List<Guideline>> entry) {
        String drug = entry.getKey();
        List<Guideline> applying = entry.getValue();

        Map<Guideline, List<Guideline>> originalToDuplicates = getOriginalToDuplicateGuidelines(applying);
        removeDuplicateGuidelines(applying, originalToDuplicates);

        result.append("\n<li data-filtertext='" + Drug.getFilterText(drug) + "'><div data-role='collapsible'>");
        result.append("\n<h3>" + drug + "</h3>");
        for(Guideline g : applying) {
            addGuidelineFieldset(result, g, originalToDuplicates);
        }
        result.append("</div></li>");
    }

    private void addGuidelineFieldset(StringBuilder result, Guideline g, Map<Guideline, List<Guideline>> originalToDuplicates) {
        result.append("<fieldset style='maring-bottom:20px'>");
        result.append("<div class='ui-bar ui-bar-e'>");
        if(!originalToDuplicates.containsKey(g)) {
            result.append("<div class='recommendation-small-text'>Reason: " + g.getPhenotypeDisplayName() + "</div>");
        } else if(originalToDuplicates.containsKey(g)) {
            result.append("<div class='recommendation-small-text'>Reasons: " + g.getPhenotypeDisplayName());
            for(Guideline duplicate : originalToDuplicates.get(g)) {
                result.append("; " + duplicate.getPhenotypeDisplayName());
            }
            result.append("</div>");
        }
        result.append(g.getRecommendation());
        result.append("<div class='recommendation-small-text'>Last guideline update: " + g.getEvidenceDate() + "</div>");
        result.append("</div>");
        result.append("<div><a href='" + g.getEvidenceUrl() + "' data-role='button' data-mini='true' " +
                "data-inline='true' data-icon='info' target='_blank'>" +
                "Show guideline website</a><span title='" + g.getName() + "'>&nbsp;</span></div>");
        result.append("</fieldset>");
    }

    /**
     * Takes a list of guidelines and a map of original guidelines to lists of guidelines that are duplicates of it.
     * Removes all duplicate guidelines from the original list.
     * @param applying A list of guidelines from which duplicates will be removed
     * @param originalToDuplicates A map where the keys are original guidelines and values are duplicates of that guideline
     */
    private void removeDuplicateGuidelines(List<Guideline> applying, Map<Guideline, List<Guideline>> originalToDuplicates) {
        for(List<Guideline> l : originalToDuplicates.values())
            for(Guideline g : l)
                applying.remove(g);
    }

    /**
     * Returns a map of every original guideline to other guidelines in the list that are duplicates of it,
     * i.e. they have the same recommendation text.
     * @param guidelines The list of guidelines.
     * @return A map of original guidelines to guidelines with the same recommendation text
     */
    private Map<Guideline, List<Guideline>> getOriginalToDuplicateGuidelines(List<Guideline> guidelines) {
        List<Guideline> originals = new ArrayList<Guideline>();
        Map<Guideline, List<Guideline>> originalToDuplicates = new HashMap<Guideline, List<Guideline>>();
        Boolean found;
        for(Guideline current : guidelines) {
            found = false;
            for(Guideline original : originals) {
                if (current.getRecommendation().equals(original.getRecommendation())) {
                    if(!originalToDuplicates.containsKey(original)) {
                        originalToDuplicates.put(original, new ArrayList<Guideline>());
                    }
                    originalToDuplicates.get(original).add(current);
                    found = true;
                }
            }
            if (!found)
                originals.add(current);
        }

        return originalToDuplicates;
    }

    /**
     * Returns the HTML describing the phenotypes and haplotypes of this profile.
     * @return the HTML for phenotypes and haplotypes
     */
    private String getGeneticDataAsHtml() {
        return getGeneTable();
    }

    private String getGeneTable() {
        StringBuilder result = new StringBuilder();
        prepareGeneTable(result);
        fillGeneTable(result);
        finishGeneTable(result);
        return result.toString();
    }

    private void prepareGeneTable(StringBuilder result) {
        result.append("<table data-role='table' class='ui-responsive'>");
        result.append("<thead><tr><th>Gene</th><th>Phenotype</th><th>Haplotype 1</th><th>Haplotype 2</th></tr></thead><tbody>");
    }

    private void fillGeneTable(StringBuilder result) {
        int i = 0;
        for(Gene gene : this.profile.getGenes()) {
            result.append("<tr><td>" + gene.getSymbol() + "</td>");
            result.append("<td>" + profile.getPhenotypes().get(i).getDisplayName() + "</td>");
            result.append("<td>" + profile.getHaplotypes().get(i * 2).getDisplayName() + "</td>");
            result.append("<td>" + profile.getHaplotypes().get(i * 2 + 1).getDisplayName() + "</td>");
            result.append("</tr>");
            i++;
        }
    }

    private void finishGeneTable(StringBuilder result) {
        result.append("</tbody></table>");
    }


    /**
     * Returns the HTML content of the template file for this resource.
     * @return the HTML content of the template file
     * @throws IOException when the file is not found
     */
    private String getHtmlTemplate() throws IOException {
        URL url = Common.getResource(Common.PROFILE_TEMPLATE);
        String templateString = IOUtils.toString(url.openStream());

        return templateString;
    }
}
