/*
 Copyright (C) 2015  Matthias Samwald

 This software collection (genomic-cds) is available under a dual licensing
 whereby use of the software in projects that are licensed so as to be compatible
 with AGPL Version 3 may use the software under the terms of that license.
 See LICENSE.md or contact matthias.samwald@meduniwien.ac.at for further details
 on the dual licensing.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.safetycode.msc3.resources;

import org.apache.commons.io.IOUtils;
import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;
import org.safetycode.msc3.representation.*;
import org.safetycode.msc3.util.Common;

import java.io.IOException;
import java.net.URL;

/**
 * Created by Sebastian on 21.12.2015.
 */
public class GenerateServerResource extends ServerResource {

    /**
     * A global StringBuilder used to put together the content that goes into the template HTML.
     */
    private StringBuilder content;
    /**
     * The final HTML that is displayed for this page.
     */
    private String html;

    @Override
    protected void doInit() {
        try {
            this.html = getHtmlTemplate().replace("${content}", getContent());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Post
    public Representation post(Representation entity) {
        String geneticUrlFragment = getGeneticUrlFragment(new Form(entity));

        String profileUrl = getProfileUrl(geneticUrlFragment);
        String pocketCardUrl = getPocketCardUrl(geneticUrlFragment);

        return getResponse(profileUrl, pocketCardUrl);
    }

    private StringRepresentation getResponse(String profileUrl, String pocketCardUrl) {
        String content = getResponseContent(profileUrl, pocketCardUrl);

        try {
            return new StringRepresentation(getHtmlTemplate().replace("${content}", content));
        } catch (IOException e) {
            e.printStackTrace();
            return new StringRepresentation("<html><body>An error occured!</body></html>");
        }
    }

    private String getResponseContent(String profileUrl, String pocketCardUrl) {
        return String.format(
                "<html><body><div style=\"text-align: center;\"" +
                        "<p>Profile: <a href=\"%s\">%s</a></p>" +
                        "<p>Pocket card: <a href=\"%s\">%s</a></p>" +
                        "</body></html>",
                profileUrl, profileUrl, pocketCardUrl, pocketCardUrl);
    }

    private String getPocketCardUrl(String geneticUrlFragment) {
        return Common.URL_PREFIX + "cards/00/" + geneticUrlFragment;
    }

    private String getProfileUrl(String geneticUrlFragment) {
        return Common.URL_PREFIX + "00/" + geneticUrlFragment;
    }

    private String getGeneticUrlFragment(Form form) {
        StringBuilder result = new StringBuilder();
        for (Gene gene : MSCManager.getInstance().getGenes()) {
            String symbol = gene.getSymbol();
            if (form.getNames().contains("checkbox-" + symbol)) {
                result.append(form.getFirstValue("checkbox-" + symbol))
                        .append(form.getFirstValue(symbol + "-phenotype"))
                        .append(form.getFirstValue(symbol + "-haplotype-1"))
                        .append(form.getFirstValue(symbol + "-haplotype-2"));
            }
        }

        return result.toString();
    }

    /**
     * Returns the final HTML that is displayed for this page.
     * @return
     * @throws IOException
     */
    @Get("html")
    public String getHtml() throws IOException {
        return html;
    }

    /**
     * Returns the String that replaces ${content} in the template HTML.
     * @return
     * @throws IOException
     */
    private String getContent() throws IOException {
        if (content == null) {
            prepareContent();
            addGenes();
            finishContent();
        }
        return content.toString();
    }

    private void prepareContent() {
        content = new StringBuilder();
        content.append("<div data-role='content' class='jqm-content'>\n");
        content.append("<form method='POST'>\n");
    }

    private void addGenes() {
        for (Gene gene : MSCManager.getInstance().getGenes()) {
            addGene(gene);
        }
    }

    private void finishContent() {
        content.append("<input type='submit' value='Submit'>");
        content.append("</form>\n");
        content.append("</div>\n");
    }

    private void addGene(Gene gene) {
        prepareGene(gene);
        addPhenotypesDropdown(gene);
        addHaplotypesDropdowns(gene);
        finishGene();
    }

    private void prepareGene(Gene gene) {
        String name = gene.getSymbol();
        String urlFragment = gene.getUrlFragment();
        content.append("<fieldset data-role='controlgroup' data-type='horizontal'>\n");
        content.append(String.format(
                "<label style='width: 100px;'>\n" +
                "   <input type='checkbox' name='checkbox-%s' value='%s' data-mini='true'>%s\n" +
                "</label>\n",
                name, urlFragment, name));
    }

    private void finishGene() {
        content.append("</fieldset>\n\n");
    }

    private void addPhenotypesDropdown(Gene gene) {
        content.append(String.format(
                "<select data-mini='true' name='%s-phenotype'>\n", gene.getSymbol()));
        for(Phenotype phenotype : gene.getPhenotypes()) {
            addPhenotypeOptions(phenotype);
        }
        content.append("</select>\n");
    }

    private void addPhenotypeOptions(Phenotype phenotype) {
        String name = phenotype.getName();
        String urlFragment = phenotype.getUrlFragment();
        content.append(String.format(
                "   <option value='%s'>%s</option>\n",
                urlFragment, name));
    }

    private void addHaplotypesDropdowns(Gene gene) {
        for (int i = 1; i <= 2; i++) {
            addHaplotypesDropdown(gene, i);
        }
    }

    private void addHaplotypesDropdown(Gene gene, int i) {
        content.append(String.format(
                "<select data-mini='true' name='%s-haplotype-%d'>\n", gene.getSymbol(), i));
        for (Haplotype haplotype : gene.getHaplotypes()) {
            addHaplotypeOptions(haplotype);
        }
        content.append("</select>\n");
    }

    private void addHaplotypeOptions(Haplotype haplotype) {
        String name = haplotype.getName();
        String urlFragment = haplotype.getUrlFragment();
        content.append(String.format(
                "   <option value='%s'>%s</option>\n",
                urlFragment, name));
    }

    private String getHtmlTemplate() throws IOException {
        URL url = Common.getResource(Common.GENERATE_TEMPLATE);
        String templateString = IOUtils.toString(url.openStream());
        return templateString;
    }
}
