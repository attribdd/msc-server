/*
 Copyright (C) 2015  Matthias Samwald

 This software collection (genomic-cds) is available under a dual licensing
 whereby use of the software in projects that are licensed so as to be compatible
 with AGPL Version 3 may use the software under the terms of that license.
 See LICENSE.md or contact matthias.samwald@meduniwien.ac.at for further details
 on the dual licensing.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.safetycode.msc3.representation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Sebastian on 16.12.2015.
 */
public class Drug {
    public static List<Drug> drugs = new ArrayList<Drug>();
    public static Map<String, Drug> drugsByName = new HashMap<String, Drug>();
    private String name;
    private String filterText;

    public static Drug get(String name) {
        return drugsByName.get(name);
    }

    /**
     * Returns the filterText for the drug with the given name, or the name itself if such a drug does not exist.
     * @param name
     * @return
     */
    public static String getFilterText(String name) {
        Drug drug = drugsByName.get(name);
        return drug == null ? name : drug.getFilterText();
    }

    public String getFilterText() { return this.filterText; }

    public Drug(String name, String filterText) {
        this.name = name;
        this.filterText = filterText;

        drugs.add(this);
        drugsByName.put(name, this);
    }

}
