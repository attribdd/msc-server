package org.safetycode.msc3.representation;

import org.safetycode.msc3.util.Common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Sebastian on 27.05.2015.
 */
public class Phenotype implements Marker, Comparable {
    /**
     * Caches phenotypes.
     */
    private static List<Phenotype> phenotypes = new ArrayList<Phenotype>();
    /**
     * Caches phenotypes by their name.
     */
    private static Map<String, Phenotype> phenotypesById = new HashMap<String, Phenotype>();

    public static Phenotype get(String id) { return phenotypesById.get(id); }

    /**
     * A unique identifier for this phenotype, e.g. CYP2C9_phenotype_0
     */
    private String id;
    /**
     * The name of the phenotype, human-readable and pretty.
     */
    private String name;
    /**
     * The URL fragment of the Phenotype, a single digit String.
     */
    private String urlFragment;
    /**
     * The gene this phenotype belongs to.
     */
    private Gene gene;
    /**
     * Is this the default phenotype for the given gene?
     */
    private boolean isDefault;
    /**
     * The gene activity/metabolizer type as displayed on the pocket card.
     * e.g. "Ultrarapid metabolizer", without gene name or other additional information.
     */
    private String displayName;
    /**
     * The list of drugs that are affected by this phenotype.
     */
    private List<String> affectedDrugs;

    @Override
    public Gene getGene() {
        return gene;
    }
    public String getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getUrlFragment() {
        return urlFragment;
    }
    public boolean isDefault() { return isDefault; }
    public String getDisplayName() {
        return displayName;
    }
    public static List<Phenotype> getPhenotypes() { return phenotypes; }
    public static Phenotype getPhenotype(String id) {
        for (Phenotype p : phenotypes)
            if (p.getId().equals(id))
                return p;
        return null;
    }
    public List<String> getAffectedDrugs() { return affectedDrugs; }


    /**
     * Constructs a phenotype with the given name for the given gene.
     * @param name the name of the phenotype
     * @param gene the gene the phenotype belongs to
     */
    Phenotype(String name, Gene gene, String urlFragment, String id, String displayName) {
        this.name = name;
        this.gene = gene;
        this.urlFragment = urlFragment;
        this.id = id;
        this.isDefault = urlFragment.equals("0") ? true : false;
        this.displayName = displayName;
        this.affectedDrugs = computeAffectedDrugs();

        gene.addPhenotype(this);

        phenotypes.add(this);
        phenotypesById.put(id, this);
    }


    /**
     * Returns the drugs that are affected by this gene.
     * To be included, the gene has to:
     *  - occur in the haplotype rule of any guideline for this drug
     *  - the guideline modification type has to be an important modification
     * @return  The list of drugs
     */
    private List<String> computeAffectedDrugs() {
        ArrayList<String> drugs = new ArrayList<String>();
        for(Guideline guideline : Guideline.getGuidelines()) {
            // Add all drugs that have this phenotype appear in the haplotype rules and have important modifications
            if(!drugs.contains(guideline.getDrug())
                    && guideline.getModificationType().equals(Common.IMPORTANT_MODIFICATION)
                    && guideline.getPhenotypeRule().contains(this.getId())) {
                drugs.add(guideline.getDrug());
            }
        }

        return drugs;
    }

    @Override
    public String toString() {
        return this.id;
    }

    @Override
    public int compareTo(Object o) {
        String a = this.urlFragment;
        String b = ((Phenotype) o).getUrlFragment();

        for(int i = 0; i < a.length(); i++) {
            int x = Common.BASE_DIGITS.indexOf(a.charAt(i));
            int y = Common.BASE_DIGITS.indexOf(b.charAt(i));

            if (x < y) return -1;
            if (y < x) return 1;
        }

        return 0;
    }

    public static class PhenotypeBuilder {
        private String name;
        private Gene gene;
        private String urlFragment;
        private String id;
        private String displayName;

        public PhenotypeBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public PhenotypeBuilder setGene(Gene gene) {
            this.gene = gene;
            return this;
        }

        public PhenotypeBuilder setUrlFragment(String urlFragment) {
            this.urlFragment = urlFragment;
            return this;
        }

        public PhenotypeBuilder setId(String id) {
            this.id = id;
            return this;
        }

        public PhenotypeBuilder setDisplayName(String displayName) {
            this.displayName = displayName;
            return this;
        }

        public Phenotype build() {
            return new Phenotype(name, gene, urlFragment, id, displayName);
        }
    }
}
