/*
 Copyright (C) 2015  Matthias Samwald

 This software collection (genomic-cds) is available under a dual licensing
 whereby use of the software in projects that are licensed so as to be compatible
 with AGPL Version 3 may use the software under the terms of that license.
 See LICENSE.md or contact matthias.samwald@meduniwien.ac.at for further details
 on the dual licensing.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.safetycode.msc3.representation;

import org.safetycode.msc3.populate.*;
import org.safetycode.msc3.util.Common;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Sebastian on 20.05.2015.
 */
public class MSCManager {
    /**
     * The singleton instance.
     */
    private static MSCManager instance;

    /**
     * Initializes and exposes Gene instances.
     */
    private GeneManager geneManager;
    /**
     * Initializes and exposes Haplotype instances.
     */
    private HaplotypeManager haplotypeManager;
    /**
     * Initializes and exposes Phenotype instances.
     */
    private PhenotypeManager phenotypeManager;
    /**
     * Initializes and exposes Guideline instances.
     */
    private GuidelineManager guidelineManager;

    private DrugManager drugManager;


    public static MSCManager getInstance() {
        if (instance == null) {
            try {
                instance = new MSCManager();
            } catch (IOException e) {
                System.err.println("Could not create MSCManager!");
                e.printStackTrace();
            }
        }
        return instance;
    }

    private GeneManager getGeneManager() throws IOException {
        if (this.geneManager == null) {
            this.geneManager = new GeneManager();
        }
        return geneManager;
    }

    private HaplotypeManager getHaplotypeManager() throws IOException {
        if (this.haplotypeManager == null) {
            this.haplotypeManager = new HaplotypeManager();
        }
        return haplotypeManager;
    }

    private PhenotypeManager getPhenotypeManager() throws IOException {
        if (this.phenotypeManager == null) {
            this.phenotypeManager = new PhenotypeManager();
        }
        return phenotypeManager;
    }

    private GuidelineManager getGuidelineManager() throws IOException {
        if (this.guidelineManager == null) {
            this.guidelineManager = new GuidelineManager();
        }
        return guidelineManager;
    }

    private DrugManager getDrugManager() throws IOException {
        if (this.drugManager == null) {
            this.drugManager = new DrugManager();
        }
        return drugManager;
    }

    public List<Guideline> getGuidelines() {
        return Guideline.getGuidelines();
    }

    public List<Guideline> getImportantGuidelines() {
        return Guideline.getImportantGuidelines();
    }

    public List<Gene> getGenes() {
        return Gene.getGenes();
    }

    public Gene getGene(String symbol) {
        return Gene.getBySymbol(symbol);
    }

    public Set<String> getDrugs() {
        TreeSet drugs = new TreeSet();
        for(Guideline g : Guideline.getGuidelines()) {
            drugs.add(g.getDrug());
        }
        return drugs;
    }

    public List<Haplotype> getHaplotypes() {
        return Haplotype.getHaplotypes();
    }

    public Haplotype getHaplotype(String id) {
        return Haplotype.getHaplotype(id);
    }

    public List<Phenotype> getPhenotypes() {
        return Phenotype.getPhenotypes();
    }

    public Phenotype getPhenotype(String id) {
        return Phenotype.getPhenotype(id);
    }


    /**
     * Constructs the MSCManager.
     */
    private MSCManager() throws IOException {
        instance = this;
        this.guidelineManager = getGuidelineManager();
        this.geneManager = getGeneManager();
        this.haplotypeManager = getHaplotypeManager();
        this.phenotypeManager = getPhenotypeManager();
        this.drugManager = getDrugManager();
        sortEverything();
    }

    private void sortEverything() {
        Gene.sortGenes();
        sortHaplotypes();
        sortPhenotypes();
    }

    private void sortHaplotypes() {
        for (Gene gene : MSCManager.getInstance().getGenes()) {
            gene.sortHaplotypes();
        }
    }

    private void sortPhenotypes() {
        for (Gene gene : MSCManager.getInstance().getGenes()) {
            gene.sortPhenotypes();
        }
    }

    /**
     * A helper class that manages Gene instances.
     */
    private class GeneManager {
        public GeneManager() throws IOException {
            initGenes();
        }

        private void initGenes() throws IOException {
            new GenePopulator(Common.GENES_PROPERTIES).populate();
        }
    }

    /**
     * A helper class that manages Haplotype instances.
     */
    private class HaplotypeManager {
        public HaplotypeManager() throws IOException {
            initHaplotypes();
        }

        private void initHaplotypes() throws IOException {
            new HaplotypePopulator(Common.HAPLOTYPES_PROPERTIES).populate();
        }
    }

    /**
     * A helper class that manages Phenotype instances.
     */
    private class PhenotypeManager {
        public PhenotypeManager() throws IOException {
            initPhenotypes();
        }

        private void initPhenotypes() throws IOException {
            new PhenotypePopulator(Common.PHENOTYPES_PROPERTIES).populate();
        }
    }

    /**
     * A helper class that manages Guideline instances.
     */
    private class GuidelineManager {
        public GuidelineManager() throws IOException {
            initGuidelines();
        }

        private void initGuidelines() throws IOException {
            new GuidelinePopulator(Common.GUIDELINES_PROPERTIES).populate();
        }
    }

    private class DrugManager {
        public DrugManager() throws IOException {
            initDrugs();
        }

        private void initDrugs() throws IOException {
            new DrugPopulator(Common.DRUGS_PROPERTIES).populate();
        }
    }
}
