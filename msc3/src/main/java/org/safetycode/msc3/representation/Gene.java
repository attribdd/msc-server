package org.safetycode.msc3.representation;

import org.safetycode.msc3.util.Common;

import java.util.*;

/**
 * Created by Sebastian on 19.05.2015.
 */
public class Gene implements Comparable {
    /**
     * Caches genes.
     */
    private static List<Gene> genes = new ArrayList<Gene>();
    /**
     * Caches genes by their gene symbol.
     */
    private static Map<String, Gene> genesBySymbol = new HashMap<String, Gene>();
    /**
     * Caches genes by their urlFragment.
     */
    private static Map<String, Gene> genesByUrlFragment = new HashMap<String, Gene>();

    /**
     * The symbol of the gene, e.g. "CYP2C9".
     */
    private final String symbol;
    /**
     * The urlFragment identifying the gene.
     */
    private final String urlFragment;
    /**
     * The haplotypes of this gene.
     */
    private List<Haplotype> haplotypes = new ArrayList<Haplotype>();
    /**
     * The possible phenotypes of this gene.
     */
    private List<Phenotype> phenotypes = new ArrayList<Phenotype>();
    /**
     * A Map of urlFragment Strings to the phenotype identified by it.
     */
    private Map<String, Phenotype> phenotypesByUrlFragment = new HashMap<String, Phenotype>();
    /**
     * A map of urlFragment Strings to the haplotype identified by it.
     */
    private Map<String, Haplotype> haplotypesByUrlFragment = new HashMap<String, Haplotype>();

    public static List<Gene>        getGenes() {
        return genes;
    }
    public String                   getUrlFragment() {
        return this.urlFragment;
    }
    public String                   getSymbol() {
        return symbol;
    }
    public List<Phenotype>          getPhenotypes() {
        return phenotypes;
    }
    public List<Haplotype>          getHaplotypes() {
        return haplotypes;
    }
    public Phenotype                getPhenotype(String urlFragment) {
        return phenotypesByUrlFragment.get(urlFragment);
    }
    public Haplotype                getHaplotype(String urlFragment) {
        return haplotypesByUrlFragment.get(urlFragment);
    }


    /**
     * Constructs a gene.
     * @param symbol        The gene symbol
     * @param urlFragment   The urlFragment that identifies the gene
     * @param haplotypes    A list of Haplotype objects for this gene
     * @param phenotypes    A list of Phenotype objects for this gene
     */
    private Gene(String symbol,
                 String urlFragment,
                 List<Haplotype> haplotypes,
                 List<Phenotype> phenotypes) {
        this.symbol = symbol;
        this.urlFragment = urlFragment;
        this.haplotypes = haplotypes;
        this.phenotypes = phenotypes;

        genesBySymbol.put(symbol, this);
        genesByUrlFragment.put(urlFragment, this);
        genes.add(this);
    }

    /**
     * Gets the Gene instance identified by symbol.
     * @param symbol the symbol identifying the gene
     * @return the gene
     */
    public static Gene getBySymbol(String symbol) {
        return genesBySymbol.get(symbol);
    }

    /**
     * Returns the gene identified by the urlFragment String.
     * @param urlFragment the urlFragment identifying the gene
     * @return the gene
     */
    public static Gene get(String urlFragment) {
        return genesByUrlFragment.get(urlFragment);
    }

    /**
     * Adds a haplotype to the haplotypes available for this gene.
     * @param haplotype the haplotype to be added
     */
    public void addHaplotype(Haplotype haplotype) {
        if (haplotype.getUrlFragment() != null) {
            haplotypesByUrlFragment.put(haplotype.getUrlFragment(), haplotype);
            this.haplotypes.add(haplotype);
        } else {
            System.err.println("Found haplotype without urlFragment: " + haplotype);
        }
    }

    /**
     * Adds a phenotype to the phenotypes available for this gene.
     * @param phenotype the phenotype to be added
     */
    public void addPhenotype(Phenotype phenotype) {
        if (phenotype.getUrlFragment() != null) {
            phenotypesByUrlFragment.put(phenotype.getUrlFragment(), phenotype);
            this.phenotypes.add(phenotype);
        } else {
            System.err.println("Found phenotype without urlFragment: " + phenotype);
        }
    }


    /**
     * Sorts the genes variable by the urlFragment.
     */
    public static void sortGenes() {
        Collections.sort(genes);
    }

    public void sortHaplotypes() {
        Collections.sort(haplotypes);
    }

    public void sortPhenotypes() {
        Collections.sort(phenotypes);
    }

    public String toString() {
        return this.symbol;
    }

    @Override
    public int compareTo(Object o) {
        String a = this.urlFragment;
        String b = ((Gene) o).getUrlFragment();

        for(int i = 0; i < a.length(); i++) {
            int x = Common.BASE_DIGITS.indexOf(a.charAt(i));
            int y = Common.BASE_DIGITS.indexOf(b.charAt(i));

            if (x < y) return -1;
            if (y < x) return 1;
        }

        return 0;
    }

    public static class GeneBuilder {
        private String symbol;
        private String urlFragment;
        public List<Haplotype> haplotypes = new ArrayList<Haplotype>();
        public List<Phenotype> phenotypes = new ArrayList<Phenotype>();

        public GeneBuilder setSymbol(String symbol) {
            this.symbol = symbol;
            return this;
        }

        public GeneBuilder setUrlFragment(String urlFragment) {
            this.urlFragment = urlFragment;
            return this;
        }

        public Gene build() {
            return new Gene(symbol, urlFragment, haplotypes, phenotypes);
        }
    }

}
